USE [Concesionaria]
GO
/****** Object:  Table [dbo].[Autos]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Autos](
	[Patente] [varchar](10) NOT NULL,
	[Año] [int] NOT NULL,
	[Marca] [varchar](10) NOT NULL,
	[Modelo] [varchar](10) NOT NULL,
	[Precio] [decimal](18, 0) NOT NULL,
	[Dni] [int] NULL,
	[Dueño] [bit] NOT NULL,
 CONSTRAINT [PK_Autos] PRIMARY KEY CLUSTERED 
(
	[Patente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[Nombre] [nchar](20) NULL,
	[Apellido] [nchar](20) NULL,
	[Dni] [int] NOT NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[Dni] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'aa400we', 2017, N'Ford', N'Fiesta', CAST(900000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'ad124sd', 2019, N'Ford', N'Focus', CAST(120000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'ad566re', 2019, N'2019', N'Palio', CAST(2019 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'adf455D', 1999, N'renault', N'clio', CAST(120000 AS Decimal(18, 0)), NULL, 0)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'apb488', 1995, N'Ford', N'ORion', CAST(60000 AS Decimal(18, 0)), 33, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'dddddd', 44444, N'zzzzzz', N'zzzzz', CAST(55555 AS Decimal(18, 0)), 33, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'j', 12, N'pepe', N'mos', CAST(13 AS Decimal(18, 0)), 33, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'jg', 12, N'pepe', N'mos', CAST(13 AS Decimal(18, 0)), 4343, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'JVL380', 2010, N'Peugeot', N'207', CAST(450000 AS Decimal(18, 0)), NULL, 0)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'rt', 2, N'2', N'b', CAST(2 AS Decimal(18, 0)), 33, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'rtw', 2, N'2', N'b', CAST(2 AS Decimal(18, 0)), 444, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'z', 2, N'2', N'z', CAST(2 AS Decimal(18, 0)), 12345, 1)
INSERT [dbo].[Autos] ([Patente], [Año], [Marca], [Modelo], [Precio], [Dni], [Dueño]) VALUES (N'zrv188', 1980, N'Ford', N'Falcon', CAST(110000 AS Decimal(18, 0)), NULL, 0)
GO
INSERT [dbo].[Clientes] ([Nombre], [Apellido], [Dni]) VALUES (N'Fernando            ', N'Toros               ', 1)
INSERT [dbo].[Clientes] ([Nombre], [Apellido], [Dni]) VALUES (N'Francisco           ', N'Sanchez             ', 33)
INSERT [dbo].[Clientes] ([Nombre], [Apellido], [Dni]) VALUES (N'Andres              ', N'Perez               ', 444)
INSERT [dbo].[Clientes] ([Nombre], [Apellido], [Dni]) VALUES (N'adasd               ', N'asfasf              ', 4343)
INSERT [dbo].[Clientes] ([Nombre], [Apellido], [Dni]) VALUES (N'pepe                ', N'argento             ', 12345)
INSERT [dbo].[Clientes] ([Nombre], [Apellido], [Dni]) VALUES (N'rrrrr               ', N'gggggg              ', 33333)
GO
ALTER TABLE [dbo].[Autos]  WITH CHECK ADD  CONSTRAINT [FK_Autos_Clientes] FOREIGN KEY([Dni])
REFERENCES [dbo].[Clientes] ([Dni])
GO
ALTER TABLE [dbo].[Autos] CHECK CONSTRAINT [FK_Autos_Clientes]
GO
/****** Object:  StoredProcedure [dbo].[AsignarDueño]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AsignarDueño]   
 
 @Dni int, @Pant varchar(10) 

AS
begin


	UPDATE Autos
	SET Dni = @Dni,Dueño = 1
	WHERE Patente = @Pant
end
GO
/****** Object:  StoredProcedure [dbo].[BorrarAuto]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BorrarAuto]   
 
 @Pant varchar(10) 

AS
begin

	DELETE FROM Autos
	WHERE Patente = @Pant
end
GO
/****** Object:  StoredProcedure [dbo].[BorrarCliente]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BorrarCliente]   
 
 @Dni int

AS
begin

	DELETE FROM Clientes
	WHERE Dni = @Dni
end
GO
/****** Object:  StoredProcedure [dbo].[DesasignarDueño]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DesasignarDueño]   
 
 @Pant varchar(10) 

AS
begin


	UPDATE Autos
	SET Dni = NULL,Dueño = 0
	WHERE Patente = @Pant
end
GO
/****** Object:  StoredProcedure [dbo].[InsertAuto]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertAuto]   
 
 @Pant varchar(10) , @Año int , @Mod varchar(20) , @Marca varchar(20) , @prec decimal

AS
begin



	INSERT INTO Autos (Patente,Año,Marca,Modelo,Precio,Dueño) VALUES (@Pant, @Año, @Marca,@Mod,@prec,0)

end
GO
/****** Object:  StoredProcedure [dbo].[InsertCliente]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertCliente]   
 
 @Dni int, @Nomb varchar(20), @Ape varchar(20)
AS
begin



	INSERT INTO Clientes(dni,Nombre,Apellido) VALUES (@Dni, @Nomb, @Ape)

end
GO
/****** Object:  StoredProcedure [dbo].[ListarAutos]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListarAutos]   
 
AS   

    SET NOCOUNT ON;  
    SELECT *
	FROM Autos
GO
/****** Object:  StoredProcedure [dbo].[ListarClientes]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListarClientes]   
 
AS   

    SET NOCOUNT ON;  
    SELECT *
	FROM Clientes
GO
/****** Object:  StoredProcedure [dbo].[ListarDueño]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ListarDueño]   
 
 @dni int

AS
begin

		SELECT a.Patente,a.Año,a.Marca, a.Modelo, a.Precio
		from Autos a inner join Clientes c on a.dni = c.Dni
		where c.Dni = @dni
end
GO
/****** Object:  StoredProcedure [dbo].[ModificarAuto]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ModificarAuto]   
 
 @PantOrig varchar(10), @Pant varchar(10) , @Año int , @Mod varchar(20) , @Marca varchar(20) , @prec decimal

AS
begin



	UPDATE Autos
	SET Patente = @Pant,Año =@Año,Marca =@Marca,Modelo = @Mod,Precio = @prec
	WHERE Patente = @PantOrig	

end
GO
/****** Object:  StoredProcedure [dbo].[ModificarCliente]    Script Date: 10/09/2020 16:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ModificarCliente]   
 
 @Dni int, @Nomb varchar(20) , @Ape varchar(20)

AS
begin



	UPDATE Clientes
	SET Dni = @Dni, Nombre = @Nomb, Apellido = @Ape
	WHERE Dni = @Dni

end
GO
