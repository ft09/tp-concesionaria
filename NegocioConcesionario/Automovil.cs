﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concesionaria
{
    public class Automovil
    {
        private string patente;

        public string Pantente
        {
            get { return patente; }
            set { patente = value; }
        }

        private int año;

        public int Año
        {
            get { return año; }
            set { año = value; }
        }
        private string modelo;

        public string Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        private double precio;

        public double Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        public static List<Automovil> ListarTodos()
        {
            List<Automovil> lista = new List<Automovil>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("ListarAutos");
            acceso.Cerrar();

            //tabla.Columns[1].ColumnName = "Autos";
                foreach (DataRow registro in tabla.Rows)
                {
                    Automovil auto = new Automovil();
                    auto.patente = registro["Patente"].ToString();
                    auto.año = Convert.ToInt32(registro["Año"].ToString());
                    auto.marca = registro["Marca"].ToString();
                    auto.modelo = registro["Modelo"].ToString();
                    auto.precio = Convert.ToDouble(registro["Precio"].ToString());

                    lista.Add(auto);
                }         
            return lista;
        }

        void agregar()
        {
            
        }
    }
}
