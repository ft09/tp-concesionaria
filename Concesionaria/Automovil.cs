﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concesionaria
{
    public class Automovil
    {
        private string patente;

        public string Pantente
        {
            get { return patente; }
            set { patente = value; }
        }

        private int año;

        public int Año
        {
            get { return año; }
            set { año = value; }
        }
        private string modelo;

        public string Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        private double precio;

        public double Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        private bool activo;

        public bool Activo
        {
            //get { return Activo; }
            set { Activo = value; }
        }

        Acceso acceso = new Acceso();
        public static List<Automovil> ListarTodos()
        {
            List<Automovil> lista = new List<Automovil>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("ListarAutos");
            acceso.Cerrar();
           

                foreach (DataRow registro in tabla.Rows)
                {
                    Automovil auto = new Automovil();
                    auto.patente = registro["Patente"].ToString();
                    auto.año = Convert.ToInt32(registro["Año"].ToString());
                    auto.marca = registro["Marca"].ToString();
                    auto.modelo = registro["Modelo"].ToString();
                    auto.precio = Convert.ToDouble(registro["Precio"].ToString());
                    auto.activo = Convert.ToBoolean(registro["Dueño"].ToString());
                     if (auto.activo != true)
                     {
                         lista.Add(auto);
                     }              
                }         
            return lista;
        }
        public static List<Automovil> Dueño(int _dni)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter dni = new SqlParameter();
            dni.ParameterName = "@dni";
            dni.Value = _dni;
            dni.SqlDbType = SqlDbType.Int;
            parametros.Add(dni);

            List<Automovil> listaDueño = new List<Automovil>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            
           DataTable tabla = acceso.Leer("ListarDueño",parametros);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Automovil auto = new Automovil();
                auto.patente = registro["Patente"].ToString();
                auto.año = Convert.ToInt32(registro["Año"].ToString());
                auto.marca = registro["Marca"].ToString();
                auto.modelo = registro["Modelo"].ToString();
                auto.precio = Convert.ToDouble(registro["Precio"].ToString());

                listaDueño.Add(auto);
            }
            
            return listaDueño;
        }
        public List<Automovil> TieneVehiculo(int _dni)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter dni = new SqlParameter();
            dni.ParameterName = "@dni";
            dni.Value = _dni;
            dni.SqlDbType = SqlDbType.Int;
            parametros.Add(dni);

            List<Automovil> listaDueño = new List<Automovil>();
            Acceso acceso = new Acceso();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarDueño", parametros);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Automovil auto = new Automovil();
                auto.patente = registro["Patente"].ToString();
                auto.año = Convert.ToInt32(registro["Año"].ToString());
                auto.marca = registro["Marca"].ToString();
                auto.modelo = registro["Modelo"].ToString();
                auto.precio = Convert.ToDouble(registro["Precio"].ToString());

                listaDueño.Add(auto);
            }
            if (listaDueño.Count != 0)
            {
                MessageBox.Show("No se puede eliminar al cliente porque tiene algun vehiculo asignado");
            }
            else
            {             
                BorrarCliente(_dni);
            }

            return listaDueño;
        }

        void BorrarCliente(int _dni)
        {
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter dni = new SqlParameter();
            dni.ParameterName = "@Dni";
            dni.Value = _dni;
            dni.SqlDbType = SqlDbType.Int;
            parametros.Add(dni);

            string SQL = "BorrarCliente";

            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure);

            if (resultado > 0)
            {
                MessageBox.Show("Se elimino correctamente");
            }
            else if (resultado == -1)
            {
                MessageBox.Show("Error en eliminar Cliente");
            }
            else if (resultado == -2)
            {
                MessageBox.Show("La conexion esta cerrada");
            }
            acceso.Cerrar();
        }

    }
}
