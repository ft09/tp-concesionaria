﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concesionaria
{
    public partial class AltaAuto : Form
    {
        Acceso acceso = new Acceso();
        Form1 formPri = new Form1();
        Automovil auto = new Automovil();
        public AltaAuto()
        {
            InitializeComponent();
        }

        private void btnAgregarAuto_Click(object sender, EventArgs e)
        {
            try
            {
                Agregar();
                Limpiar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
           
        }

        void Agregar()
        {
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter patente = new SqlParameter();
            patente.ParameterName = "@Pant";
            patente.Value = txtPatente.Text;
            patente.SqlDbType = SqlDbType.Text;
            parametros.Add(patente);

            SqlParameter año = new SqlParameter();
            año.ParameterName = "@Año";
            año.Value = int.Parse(txtAño.Text);
            año.SqlDbType = SqlDbType.Int;
            parametros.Add(año);

            SqlParameter modelo = new SqlParameter();
            modelo.ParameterName = "@Mod";
            modelo.Value = txtModelo.Text;
            modelo.SqlDbType = SqlDbType.Text;
            parametros.Add(modelo);

            SqlParameter marca = new SqlParameter();
            marca.ParameterName = "@Marca";
            marca.Value = txtMarca.Text;
            marca.SqlDbType = SqlDbType.Text;
            parametros.Add(marca);

            SqlParameter precio = new SqlParameter();
            precio.ParameterName = "@Prec";
            precio.Value = txtPrecio.Text;
            precio.SqlDbType = SqlDbType.Decimal;
            parametros.Add(precio);

            string SQL = "InsertAuto";


            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure);
            acceso.Cerrar();
            if (resultado > 0)
            {
                MessageBox.Show("Se dio de alta correctamente");
                Actualizar();
            }
            else if (resultado == -1)
            {
                MessageBox.Show("Error en Alta");
            }
            else if (resultado == -2)
            {
                MessageBox.Show("La conexion esta cerrada");
            }            
        }
        void Modificar(string _pantOriginal)
        {
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter patenteOri = new SqlParameter();
            patenteOri.ParameterName = "@PantOrig";
            patenteOri.Value = _pantOriginal;
            patenteOri.SqlDbType = SqlDbType.Text;
            parametros.Add(patenteOri);

            SqlParameter patente = new SqlParameter();
            patente.ParameterName = "@Pant";
            patente.Value = txtPatente.Text;
            patente.SqlDbType = SqlDbType.Text;
            parametros.Add(patente);

            SqlParameter año = new SqlParameter();
            año.ParameterName = "@Año";
            año.Value = int.Parse(txtAño.Text);
            año.SqlDbType = SqlDbType.Int;
            parametros.Add(año);

            SqlParameter modelo = new SqlParameter();
            modelo.ParameterName = "@Mod";
            modelo.Value = txtModelo.Text;
            modelo.SqlDbType = SqlDbType.Text;
            parametros.Add(modelo);

            SqlParameter marca = new SqlParameter();
            marca.ParameterName = "@Marca";
            marca.Value = txtMarca.Text;
            marca.SqlDbType = SqlDbType.Text;
            parametros.Add(marca);

            SqlParameter precio = new SqlParameter();
            precio.ParameterName = "@Prec";
            precio.Value = txtPrecio.Text;
            precio.SqlDbType = SqlDbType.Decimal;
            parametros.Add(precio);

            string SQL = "ModificarAuto";


            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure);
            acceso.Cerrar();
            if (resultado > 0)
            {
                MessageBox.Show("Se modifico correctamente");
                Actualizar();
            }
            else if (resultado == -1)
            {
                MessageBox.Show("Error en Modificar");
            }
            else if (resultado == -2)
            {
                MessageBox.Show("La conexion esta cerrada");
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                Automovil auto = (Automovil)GrillaAutos.SelectedRows[0].DataBoundItem;
                BorrarAuto(auto.Pantente);
                Actualizar();
                Limpiar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
           
        }

        void BorrarAuto(string _patente)
        {
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter patente = new SqlParameter();
            patente.ParameterName = "@Pant";
            patente.Value = _patente;
            patente.SqlDbType = SqlDbType.Text;
            parametros.Add(patente);

            string SQL = "BorrarAuto";

            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure);

            if (resultado > 0)
            {
                MessageBox.Show("Se elimino correctamente");
            }
            else if (resultado == -1)
            {
                MessageBox.Show("Error en eliminar dueño");
            }
            else if (resultado == -2)
            {
                MessageBox.Show("La conexion esta cerrada");
            }
            acceso.Cerrar();
        }
        void Actualizar()
        {
            GrillaAutos.DataSource = null;
            GrillaAutos.DataSource = Automovil.ListarTodos();
        }
        private void AltaAuto_Load(object sender, EventArgs e)
        {
            Actualizar();
        }
        void Limpiar()
        {
            txtPatente.Text = null;
            txtAño.Text = null;
            txtMarca.Text = null;
            txtModelo.Text = null;
            txtPrecio.Text = null;

        }
        private void btnModif_Click(object sender, EventArgs e)
        {
            try
            {
                Automovil auto = (Automovil)GrillaAutos.SelectedRows[0].DataBoundItem;
                string pante = auto.Pantente;
                Modificar(pante);
                Actualizar();
                Limpiar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
           
        }

        private void GrillaAutos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Automovil auto = (Automovil)GrillaAutos.SelectedRows[0].DataBoundItem;
                Cargar(auto);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Seleccione una fila");
            }
            
        }

        void Cargar(Automovil _auto)
        {
            txtPatente.Text = _auto.Pantente;
            txtAño.Text = _auto.Año.ToString();
            txtMarca.Text = _auto.Marca;
            txtModelo.Text = _auto.Modelo;
            txtPrecio.Text = _auto.Precio.ToString();
        }
    }
}
