﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Concesionaria
{
    internal class Acceso
    {
        private SqlConnection conexion;
        private SqlTransaction transaccion;
        public void Abrir()
        {
            conexion = new SqlConnection("Initial Catalog=Concesionaria; Data Source= .\\sqlexpress; Integrated Security=SSPI");
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        private SqlCommand CrearComando(string sql, List<SqlParameter> parametros = null, CommandType tipo = CommandType.Text)
        {
            SqlCommand comando = new SqlCommand(sql);
            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            comando.Connection = conexion;

            return comando;
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.String;
            return p;
        }

        public SqlParameter CrearParametro(string nombre, bool valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Boolean;
            return p;
        }

        public IDbDataParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Int32;
            return p;
        }

        public DataTable Leer(string sql, List<SqlParameter> parameters = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();

            adaptador.SelectCommand = CrearComando(sql, parameters,CommandType.StoredProcedure);

            DataTable Tabla = new DataTable();

            adaptador.Fill(Tabla);


            return Tabla;
        }
        public int Escribir(string SQL, List<SqlParameter> parametros = null, System.Data.CommandType tipo = System.Data.CommandType.Text)
        {
            int filasAfectadas;

            SqlCommand comando = new SqlCommand();

            comando.Connection = conexion;

            comando.CommandText = SQL;

            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {

                #region "variantes de agregado de parámetros"
                /*  foreach (SqlParameter p in parametros)
                  {
                      comando.Parameters.Add(p);
                  }
                 
                foreach (SqlParameter p in parametros)
               {
                   comando.Parameters.AddWithValue (p.ParameterName,p.Value);
               }
               */
                #endregion

                comando.Parameters.AddRange(parametros.ToArray());


            }
            if (transaccion != null)
            {
                comando.Transaction = transaccion;
            }

            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }

            catch (SqlException ex)
            {
                filasAfectadas = -1;
            }
            catch (Exception ex)
            {
                filasAfectadas = -2;
            }
            return filasAfectadas;
        }


    }
}
