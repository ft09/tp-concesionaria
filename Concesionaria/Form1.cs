﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Concesionaria
{
    public partial class Form1 : Form
    {
        Acceso acceso = new Acceso();
        public Form1()
        {
            InitializeComponent();
        }

        private void altaDeAutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AltaAuto frmAuto = new AltaAuto();
            frmAuto.Show();
        }

        private void altaClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AltaCliente frmCliente = new AltaCliente();
            frmCliente.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
        public void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Cliente.listar();
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = Automovil.ListarTodos();
            
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cliente cliente = (Cliente)dataGridView1.Rows[e.RowIndex].DataBoundItem;
            dataGridView3.DataSource = null;
            dataGridView3.DataSource = Automovil.Dueño(cliente.Dni);
        }

        private void btnAsignar_Click(object sender, EventArgs e)
        {
            try
            {
                Cliente cliente = (Cliente)dataGridView1.SelectedRows[0].DataBoundItem;
                Automovil auto = (Automovil)dataGridView2.SelectedRows[0].DataBoundItem;

                if (auto != null && cliente != null)
                {
                    AsignarDueño(cliente.Dni, auto.Pantente);
                    Enlazar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void AsignarDueño(int _dni, string _patente)
        {
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter dni = new SqlParameter();
            dni.ParameterName = "@dni";
            dni.Value = _dni;
            dni.SqlDbType = SqlDbType.Int;
            parametros.Add(dni);

            SqlParameter patente = new SqlParameter();
            patente.ParameterName = "@Pant";
            patente.Value = _patente;
            patente.SqlDbType = SqlDbType.Text;
            parametros.Add(patente);

            string SQL = "AsignarDueño";

            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure);
            
            if (resultado > 0)
            {
                MessageBox.Show("Se asigno correctamente");
                Enlazar();
            }
            else if (resultado == -1)
            {
                MessageBox.Show("Error en asignar dueño");
            }
            else if (resultado == -2)
            {
                MessageBox.Show("La conexion esta cerrada");
            }
            acceso.Cerrar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Automovil auto = (Automovil)dataGridView3.SelectedRows[0].DataBoundItem;
                DesasignarDueño(auto.Pantente);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString()); 
            }
            
        }

        void DesasignarDueño(string _patente)
        {
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter patente = new SqlParameter();
            patente.ParameterName = "@Pant";
            patente.Value = _patente;
            patente.SqlDbType = SqlDbType.Text;
            parametros.Add(patente);

            string SQL = "DesasignarDueño";

            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure);

            if (resultado > 0)
            {
                MessageBox.Show("Se Desasigno correctamente");
                Enlazar();
            }
            else if (resultado == -1)
            {
                MessageBox.Show("Error en asignar dueño");
            }
            else if (resultado == -2)
            {
                MessageBox.Show("La conexion esta cerrada");
            }
            acceso.Cerrar();
        }
    }
}
