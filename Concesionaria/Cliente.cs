﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concesionaria
{
    class Cliente
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private int dni;

        public int Dni
        {
            get { return dni; }
            set { dni = value; }
        }

        public static List<Cliente> listar()
        {
            List<Cliente> listClientes = new List<Cliente>();

            Acceso acceso = new Acceso();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarClientes");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Cliente cli = new Cliente();
                cli.Dni = int.Parse(registro["Dni"].ToString());
                cli.Nombre = registro["Nombre"].ToString();
                cli.Apellido = registro["Apellido"].ToString();

                listClientes.Add(cli);
            }


            return listClientes;
        }
        

    }
}
