﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concesionaria
{
    public partial class AltaCliente : Form
    {
        Acceso acceso = new Acceso();
        Form1 formPri = new Form1();
        Automovil auto = new Automovil();
        public AltaCliente()
        
        {
            InitializeComponent();
        }

        private void btnAgregarCli_Click(object sender, EventArgs e)
        {
            try
            {
                AgregarCliente();
                Actualizar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }
        void AgregarCliente()
        {
            acceso.Abrir();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter dni = new SqlParameter();
            dni.ParameterName = "@dni";
            dni.Value = txtDni.Text;
            dni.SqlDbType = SqlDbType.Int;
            parametros.Add(dni);

            SqlParameter nombre = new SqlParameter();
            nombre.ParameterName = "@nomb";
            nombre.Value = txtNombre.Text;
            nombre.SqlDbType = SqlDbType.Text;
            parametros.Add(nombre);

            SqlParameter apellido = new SqlParameter();
            apellido.ParameterName = "@ape";
            apellido.Value = txtApellido.Text;
            apellido.SqlDbType = SqlDbType.Text;
            parametros.Add(apellido);


            string SQL = "InsertCliente";

            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure);

            acceso.Cerrar();
            if (resultado > 0)
            {
                MessageBox.Show("Se dio de alta correctamente");
                formPri.Enlazar();
            }
            else if (resultado == -1)
            {
                MessageBox.Show("Error en Alta");
            }
            else if (resultado == -2)
            {
                MessageBox.Show("La conexion esta cerrada");
            }           
        }
        void BorrarCliente(string _patente)
        {
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter patente = new SqlParameter();
            patente.ParameterName = "@Pant";
            patente.Value = _patente;
            patente.SqlDbType = SqlDbType.Text;
            parametros.Add(patente);

            string SQL = "BorrarAuto";

            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure);

            if (resultado > 0)
            {
                MessageBox.Show("Se elimino correctamente");
            }
            else if (resultado == -1)
            {
                MessageBox.Show("Error en eliminar dueño");
            }
            else if (resultado == -2)
            {
                MessageBox.Show("La conexion esta cerrada");
            }
            acceso.Cerrar();
        }
        void Actualizar()
        {
            GrillaClientes.DataSource = null;
            GrillaClientes.DataSource = Cliente.listar();
        }
        private void AltaCliente_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Cliente cliente = (Cliente)GrillaClientes.SelectedRows[0].DataBoundItem;
                auto.TieneVehiculo(cliente.Dni);
                Actualizar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void GrillaClientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Cliente cliente = (Cliente)GrillaClientes.SelectedRows[0].DataBoundItem;
                Cargar(cliente);
            }
            catch (Exception)
            {

                MessageBox.Show("Seleccione un elemento de la fila");
            }
            
        }

        void Cargar(Cliente _cliente)
        {
            txtDni.Text = _cliente.Dni.ToString();
            txtNombre.Text = _cliente.Nombre;
            txtApellido.Text = _cliente.Apellido;
            
        }
        void Limpiar()
        {
            txtDni.Text = null;
            txtNombre.Text = null;
            txtApellido.Text = null;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Cliente cliente = (Cliente)GrillaClientes.SelectedRows[0].DataBoundItem;
                ModificarCliente(cliente.Dni);
                Actualizar();
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
         
        }

        void ModificarCliente(int _dni)
        {
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter dni = new SqlParameter();
            dni.ParameterName = "@Dni";
            dni.Value = _dni;
            dni.SqlDbType = SqlDbType.Int;
            parametros.Add(dni);

            SqlParameter nombre = new SqlParameter();
            nombre.ParameterName = "@Nomb";
            nombre.Value = txtNombre.Text;
            nombre.SqlDbType = SqlDbType.Text;
            parametros.Add(nombre);

            SqlParameter apellido = new SqlParameter();
            apellido.ParameterName = "@Ape";
            apellido.Value = txtApellido.Text;
            apellido.SqlDbType = SqlDbType.Text;
            parametros.Add(apellido);

            string SQL = "ModificarCliente";


            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure);
           
            if (resultado > 0)
            {
                MessageBox.Show("Se modifico correctamente");
            }
            else if (resultado == -1)
            {
                MessageBox.Show("Error en Modificar");
            }
            else if (resultado == -2)
            {
                MessageBox.Show("La conexion esta cerrada");
            }
            acceso.Cerrar();
        }

    }
}
